# Shunnel

Shunnel is simply an ssh tunnel scripted out to make it easy to save and specify various configurations. You can also check to see if you already have a tunnel open, as well as kill an existing tunnel.

## Install

Clone the repo, cp the shunnel.conf.sample to <profile>.conf and set the variables in the .conf file to get started. If no variables are set, some are missing, or no profile exists, the script will prompt you for them.

  * DEST = hostname or IP of system to tunnel into
  * DESTPORT = port that accepts ssh connections on the destination system
  * LOCALPORT = local port to bind to the ssh tunnel
  * USER = username to connect to the destination server with

## Usage

shunnel.sh [-s|--start (-p|--profile)=<profile>] [-k|--stop] [-u|--status]

<profile> should be the file name of whatever .conf file you want to use. E.g. if you have default.conf, <profile> would be replaced with 'default'