#!/bin/bash

USAGE="shunnel.sh [-s|--start (-p|--profile)=<profile>] [-k|--stop] [-u|--status]"

# Get script location so we can source any conf files
# regardless of where the script gets called.
DIR="$( cd "$( dirname "$0" )" && pwd )"

# Parse the options into the "ACTION" variable (plus set the profile)
for i in "$@"
do
case $i in
  -p=*|--profile=*)
    PRO=`echo $i | sed 's/[-a-zA-Z0-9]*=//'`
    # If a profile was specified and exists, set the "PROFILE" variable
    for f in $(dirname $DIR)/config/*.conf; do
      fname=$(basename $f)
      if [ "$PRO" == "${fname%.*}" ]; then
        source $f
        PROFILE="$PRO"
      fi
    done
    
    # If profile is specified but doesn't exist, let the user know
    if [[ -z "${PROFILE}" ]]; then
      echo "The profile you specified (${PRO}) could not be found or is undefined."
      exit 1
    fi
  ;;
  -s|--start)
    # Checks to make sure "ACTION" isn't already set, then sets it
    if [[ -z "${ACTION}" ]]; then
      ACTION="start"
    else
      echo "You cannot select more than one option!"
      exit 1
    fi
  ;;
  -k|--stop)
    # Checks to make sure "ACTION" isn't already set, then sets it
    if [[ -z "${ACTION}" ]]; then
      ACTION="stop"
    else
      echo "You cannot select more than one option!"
      exit 1
    fi   
  ;;
  -u|--status)
    # Checks to make sure "ACTION" isn't already set, then sets it
    if [[ -z "${ACTION}" ]]; then
      ACTION="status"
    else
      echo "You cannot select more than one option!"
      exit 1
    fi
  ;;
  *)
    echo ${USAGE}  
  ;;
esac
done

# Execute script depending on which action was set
if [[ $ACTION == "start" ]]; then
  # Check if there is already a PID file, which would indicate shunnel
  # is already running or was not stopped properly. If not found, move on.
  if [ ! -f $DIR/../shunnel.pid ]; then
    echo "Creating SSH tunnel..."
  else
    # If PID file is found, load it and see if it's valid, if not, remove it
    source $DIR/../shunnel.pid
    if [[ -z "${PID}" ]]; then
      echo "Found a stale PID file, cleaning up."
      rm $DIR/../shunnel.pid
    else
      # If PID file appears to be valid, confirm that the process is still
      # running, then let the user know. If process is not running, remove
      # the PID file.
      OLD_PID=$(ps aux | grep "${CMD}" | grep -v grep | awk "{ print \$2 }")
      if [[ ${OLD_PID} == ${PID} ]]; then
        echo "Shunnel is already running as '${CMD}' (process ID ${PID})!"
        exit 1
      else
        echo "Found a stale PID file, cleaning up."
        rm $DIR/../shunnel.pid
      fi
    fi
  fi

  # Check if the profile is set, if it isn't, look for existing profiles.
  # If there's one exists, use it, otherwise let the user know they will be prompted.
  if [[ -z "${PROFILE}" ]]; then
    N=0
    for f in $(dirname $DIR)/config/*.conf; do
      N=$((N+1))
    done
    if [ $N == 1 ]; then
      for f in $(dirname $DIR)/config/*.conf; do
        fname=$(basename $f)
        echo "One configuration profile was found (${fname%.*})."
        source $f
      done
    else
      echo "No configuration or multiple profiles found, you will be prompted for configuration options."
    fi
  fi

  # If any variables are not specified in the configuration profile, or
  # no profile exists, prompt the user to specify the values.
  if [[ -z "${DEST}" ]]; then
    read -p "What is the destination host or IP?" DEST
  fi

  if [[ -z "${DESTPORT}" ]]; then
    read -p "What port should we connect to (via SSH) on ${DEST}?" DESTPORT
  fi

  if [[ -z "${LOCALPORT}" ]]; then
    read -p "What port should we use for local traffic forwarding?" LOCALPORT
  fi

  if [[ -z "${USER}" ]]; then
    read -p "What user should we use to authenticate to ${DEST}?" USER
  fi

  if [[ -z "${IDENTITYFILE}" ]]; then
    read -p "What identity file should we use to authenticate as ${USER}? Leave blank for none." IDENTITYFILE
  fi

  if [[ -n "${IDENTITYFILE}" ]]; then
    idline="-i ${IDENTITYFILE}"
  fi

  # Set the ssh command to a variable so we can use it to see if it's still running
  cmd="ssh -p ${DESTPORT} -D ${LOCALPORT} -C -N ${USER}@${DEST} ${idline}"
  # Execute the ssh tunnel
  ssh -p ${DESTPORT} -D ${LOCALPORT} -C -N ${USER}@${DEST} ${idline} &
  # Save the process ID to a variable
  SHUNNEL_PID=$!
  # Let the user know what's up
  echo ""
  echo "Started shunnel at ${DEST}:${DESTPORT} on local port ${LOCALPORT}."
  echo "  * Process ID is ${SHUNNEL_PID}."
  echo "  * You can stop the ssh tunnel at anytime using 'shunnel.sh --stop'."
  echo "#!/bin/bash" >$DIR/../shunnel.pid
  echo "PID=${SHUNNEL_PID}" >>$DIR/../shunnel.pid
  echo "CMD=\"${cmd}\"" >>$DIR/../shunnel.pid
  echo ""

elif [[ $ACTION == "stop" ]]; then
  # If there's no PID file, then we don't know what to look for. If it's there
  # but doesn't have the right info, then we still don't know what to look for.
  # If the file is there, and variables are set, kill the process.
  if [ ! -f $DIR/../shunnel.pid ]; then
    echo "No shunnel process could be found to stop."
  else
    source $DIR/../shunnel.pid
    if [[ -z "${PID}" ]]; then
      echo "No valid 'shunnel.pid' file could be found, or 'shunnel.pid' file is corrupted."
    else
      kill ${PID}
      rc=$?
      # Echo success if the kill process succeeds
      if [ $rc = 0 ]; then
        echo "Shunnel (process ID ${PID}) has been stopped."
        rm -rf shunnel.pid
      else
        echo "Shunnel could not be stopped."
      fi
    fi
  fi
elif [[ $ACTION == "status" ]]; then
  # If there's no PID file, then we don't know what to look for. If it's there
  # but doesn't have the right info, then we still don't know what to look for.
  # If the file is there, and variables are set, check if it's running and report.
  # If there's no process that matches, remove the PID file. 
  if [ ! -f $DIR/../shunnel.pid ]; then
    echo "No shunnel process could be found."
  else
    source $DIR/../shunnel.pid
    if [[ -z "${PID}" ]]; then
      echo "No valid 'shunnel.pid' file could be found, or 'shunnel.pid' is corrupted."
    else
      if [ $(ps aux | grep "${CMD}" | grep -v grep | awk "{ print \$2 }") == ${PID} ]; then
        echo "Shunnel is running as ${CMD} (process ID ${PID})."
      else
        echo "No shunnel process could be found, cleaning up stale PID file."
        rm $DIR/../shunnel.pid
      fi
    fi
  fi
fi